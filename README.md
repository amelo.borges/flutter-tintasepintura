# Tintas & Pintura (DIGITAL REPUBLIC CODE CHALLENGE)

"Projetinho" desenvolvido com o objetivo de avaliar o conhecimento e capacidade dos candidatos às vagas de programação/desenvolvimento.

## Projeto desenvolvido com Flutter (versão 3.7) Dart sdk 2.19.0

Siga as instuções do link abaixo para configuração de ambiente e instalação da aplicação.

- [Configuração e Instalação](https://docs.flutter.dev/get-started/install)

## Telas da aplicação

### 1
![Terminal](https://containerproject.s3.sa-east-1.amazonaws.com/images/1.png)
### 2
![Terminal](https://containerproject.s3.sa-east-1.amazonaws.com/images/2.png)
### 3
![Terminal](https://containerproject.s3.sa-east-1.amazonaws.com/images/4.png)
### 4
![Terminal](https://containerproject.s3.sa-east-1.amazonaws.com/images/3.png)
