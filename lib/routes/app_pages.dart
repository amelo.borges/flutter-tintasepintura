import 'package:get/get.dart';
import 'package:tintasepintura/app/ui/login/login_binding.dart';
import 'package:tintasepintura/app/ui/login/login_view.dart';
import 'package:tintasepintura/app/ui/navigation_control/navigation_control_binding.dart';
import 'package:tintasepintura/app/ui/navigation_control/navigation_control_view.dart';
import 'package:tintasepintura/routes/app_routes.dart';

class AppPages {
  static final List<GetPage> pages = [

    GetPage(
        name: AppRoutes.login,
        transition: Transition.cupertino ,
        page: () => const LoginView(),
        binding: LoginBinding()),

    GetPage(
        name: AppRoutes.navigationControl,
        transition: Transition.cupertino ,
        page: () => const NavigationControlView(),
        binding: NavigationControlBinding()),

  ];
}