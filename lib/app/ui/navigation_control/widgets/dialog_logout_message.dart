import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tintasepintura/app/ui/navigation_control/navigation%20_control_view_model.dart';

class DialogLogoutMessage extends StatelessWidget {
  const DialogLogoutMessage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final vm = Get.find<NavigationControlViewModel>();
    return Material(
      color: const Color.fromRGBO(0, 0, 0, 0.4),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
            margin: const EdgeInsets.only(left: 30.0, right: 30.0),
            decoration: BoxDecoration(
              color: const Color.fromRGBO(255, 255, 255, 1.0),
              borderRadius: BorderRadius.circular(16.0),
            ),
            child: Column(
              children: [
                Center(
                  child: Text(
                    "Deseja fazer logout ?",
                    style: GoogleFonts.montserrat(
                      textStyle: const TextStyle(
                        fontSize: 20.0,
                        color: Color.fromRGBO(78, 78, 78, 1.0),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 30.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    SizedBox(
                      width: 110.0,
                      height: 34.0,
                      child: ElevatedButton(
                        onPressed: () {
                          Get.back();
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: const Color.fromRGBO(141,141,141,1.0)
                        ),
                        child: Text(
                          "Cancelar",
                          style: GoogleFonts.montserrat(
                            textStyle: const TextStyle(
                              fontSize: 15.0,
                              color: Color.fromRGBO(255, 255, 255, 1.0),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 110.0,
                      height: 34.0,
                      child: ElevatedButton(
                        onPressed: () {
                          vm.logout();
                          Get.back();
                        },
                        style: ElevatedButton.styleFrom(
                            backgroundColor: const Color.fromRGBO(26,141,255,1.0)
                        ),
                        child: Text(
                          "Sair",
                          style: GoogleFonts.montserrat(
                            textStyle: const TextStyle(
                              fontSize: 16.0,
                              color: Color.fromRGBO(255, 255, 255, 1.0),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
