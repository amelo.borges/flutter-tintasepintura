import 'package:get/get.dart';
import 'package:tintasepintura/app/ui/navigation_control/navigation%20_control_view_model.dart';
import 'package:tintasepintura/app/ui/navigation_control/navigation_control_repository.dart';

class NavigationControlBinding extends Bindings{

  @override
  void dependencies() {
    Get.lazyPut(() => NavigationControlViewModel());
    Get.lazyPut(() => NavigationControlRepository());
  }

}