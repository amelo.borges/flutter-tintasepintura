import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tintasepintura/app/ui/history/history_view.dart';
import 'package:tintasepintura/app/ui/home/home_view.dart';
import 'package:tintasepintura/app/ui/navigation_control/navigation_control_repository.dart';
import 'package:tintasepintura/app/ui/navigation_control/widgets/dialog_logout_message.dart';
import 'package:tintasepintura/core/utils/app_utils.dart';
import 'package:tintasepintura/routes/app_routes.dart';

class NavigationControlViewModel extends GetxController with AppUtils{

  final repository = Get.find<NavigationControlRepository>();

  RxInt bottomNavIndex = 0.obs;

  final List<Widget> views = [
    const HomeView(),
    const HistoryView(),
  ];

  final iconList = <IconData>[
    Icons.home,
    Icons.list,
    Icons.logout_outlined
  ];


  setNavView(int index){
    if(index == 2){
      openDialogLogout();
    }else{
      bottomNavIndex.value = index;
    }
  }


  openDialogLogout() async{
    await showDialog(context: Get.context!, builder: (context) {
      return const DialogLogoutMessage();
    });
  }


  logout() async{
    try{
      await FirebaseAuth.instance.signOut();
      repository.hiveDelete();
      Get.offNamed(AppRoutes.login);
    }catch(e){
      loggerError(message: e);
    }
  }


  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}