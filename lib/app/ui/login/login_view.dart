import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tintasepintura/app/ui/login/login_view_model.dart';
import 'package:tintasepintura/core/theme/app_theme.dart';
import 'package:tintasepintura/core/values/constants.dart';

class LoginView extends StatelessWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<LoginViewModel>(
      builder: (vm) {
        return Container(
          width: double.infinity,
          height: double.infinity,
          color: const Color.fromRGBO(0, 0, 193, 1.0),
          child: Scaffold(
            body: Stack(
              alignment: Alignment.center,
              children: [
                Image.asset(AppImages.backbround,
                    width: double.infinity,
                    height: double.infinity,
                    fit: BoxFit.cover),
                Obx(
                  () => Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(AppImages.logoApp),

                      vm.userFirebaseLoginNull.value ?  Container(
                        width: 280.0,
                        margin: const EdgeInsets.only(top: 25.0),
                        padding: const EdgeInsets.all(15.0),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(16.0),
                          boxShadow: AppTheme.shadow(
                            const Color.fromRGBO(78, 78, 70, 0.4),
                          ),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Center(
                              child: Text(
                                vm.isLogin.value ? "Login" : "Criar conta",
                                style: GoogleFonts.montserrat(
                                  textStyle: const TextStyle(
                                    fontSize: 18.0,
                                    fontWeight: FontWeight.w700,
                                    color: Color.fromRGBO(78, 78, 78, 1.0),
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(height: 10.0),
                            Text(
                              "E-mail:",
                              style: GoogleFonts.montserrat(
                                textStyle: const TextStyle(
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.w600,
                                  color: Color.fromRGBO(78, 78, 78, 1.0),
                                ),
                              ),
                            ),
                            Container(
                              width: double.infinity,
                              height: 35.0,
                              padding:
                                  const EdgeInsets.only(left: 10.0, top: 14.0),
                              decoration: BoxDecoration(
                                color: Colors.black12,
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: TextField(
                                maxLines: 1,
                                textAlign: TextAlign.start,
                                onChanged: (value) => vm.changeEmail(value),
                                keyboardType: TextInputType.emailAddress,
                                style: GoogleFonts.montserrat(
                                  textStyle: const TextStyle(
                                    fontSize: 13.0,
                                    fontWeight: FontWeight.w500,
                                    color: Color.fromRGBO(78, 78, 78, 1.0),
                                  ),
                                ),
                                decoration: const InputDecoration(
                                  hintText: "",
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                            const SizedBox(height: 10.0),
                            Text(
                              "Senha:",
                              style: GoogleFonts.montserrat(
                                textStyle: const TextStyle(
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.w600,
                                  color: Color.fromRGBO(78, 78, 78, 1.0),
                                ),
                              ),
                            ),
                            Container(
                              width: double.infinity,
                              height: 35.0,
                              margin: const EdgeInsets.only(bottom: 20.0),
                              padding:
                                  const EdgeInsets.only(left: 10.0, top: 13.0),
                              decoration: BoxDecoration(
                                color: Colors.black12,
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: TextField(
                                maxLines: 1,
                                obscureText: true,
                                onChanged: (value) => vm.changePassWord(value),
                                textAlign: TextAlign.start,
                                keyboardType: TextInputType.text,
                                style: GoogleFonts.montserrat(
                                  textStyle: const TextStyle(
                                    fontSize: 16.0,
                                    color: Color.fromRGBO(78, 78, 78, 1.0),
                                  ),
                                ),
                                decoration: const InputDecoration(
                                  hintText: "",
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                            Obx(
                              () => Center(
                                child: SizedBox(
                                  width: 110.0,
                                  height: 33.0,
                                  child: ElevatedButton(
                                    onPressed: () => vm.isValidEmail.value &&
                                            vm.isPassword.value
                                        ? vm.submit()
                                        : null,
                                    style: ElevatedButton.styleFrom(
                                      backgroundColor: Color.fromRGBO(
                                          0,
                                          0,
                                          193,
                                          vm.isValidEmail.value &&
                                                  vm.isPassword.value
                                              ? 1.0
                                              : 0.1),
                                    ),
                                    child: vm.isLoading.value ? const SizedBox(width: 25.0, height: 25.0,child: CircularProgressIndicator(color: Colors.white,strokeWidth: 2.0)) : Text(
                                      vm.isLogin.value ? "Entrar" : "Criar",
                                      style: GoogleFonts.montserrat(
                                        textStyle: const TextStyle(
                                            fontSize: 15.0,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.white),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(height: 10.0),
                            InkWell(
                              onTap: () {
                                vm.toggleAuthMode();
                              },
                              child: Center(
                                child: Text(
                                  vm.isLogin.value
                                      ? "Criar uma nova conta ?"
                                      : "Já possuo login!",
                                  style: GoogleFonts.montserrat(
                                    textStyle: const TextStyle(
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.w600,
                                      color: Color.fromRGBO(78, 78, 78, 1.0),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ) : Container(
                        margin: const EdgeInsets.only(top: 30.0),
                        child: const SizedBox(
                          child: CircularProgressIndicator(),
                        ),
                      ),

                      Obx(() => vm.messageError.value.isNotEmpty ?
                      Container(
                        width: 280.0,
                        margin: const EdgeInsets.only(top: 30.0),
                        padding: const EdgeInsets.only(left: 10.0, top: 15.0, right: 10.0, bottom: 15.0),
                        decoration: BoxDecoration(
                            color: const Color.fromRGBO(221,105,106,1.0),
                            borderRadius: BorderRadius.circular(10.0)
                        ),
                        child: Column(
                          children: [
                            Text(vm.messageError.value, style: GoogleFonts.montserrat(
                              textStyle: const TextStyle(
                                fontSize: 16.0,
                                color: Color.fromRGBO(255,255,255,1.0),
                                fontWeight: FontWeight.w500
                              )
                            ),),
                          ],
                        ),
                      ) : const SizedBox(),),

                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
