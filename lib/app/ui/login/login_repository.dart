import 'package:tintasepintura/app/data/services/auth_service.dart';
import 'package:tintasepintura/core/values/app_enum.dart';

class LoginRepository{
  final _authService = AuthService.instance;

  Future<AuthStatusMessage> signup({required String email,required String password}) => _authService.signup(email: email, password: password);
  Future<AuthStatusMessage> sign({required String email,required String password}) => _authService.sign(email: email, password: password);

}