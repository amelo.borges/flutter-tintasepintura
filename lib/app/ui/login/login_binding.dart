import 'package:get/get.dart';
import 'package:tintasepintura/app/ui/login/login_repository.dart';
import 'package:tintasepintura/app/ui/login/login_view_model.dart';

class LoginBinding extends Bindings{

  @override
  void dependencies() {
    Get.lazyPut(() => LoginRepository());
    Get.lazyPut(() => LoginViewModel());
  }

}