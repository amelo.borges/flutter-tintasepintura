import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:get/get.dart';
import 'package:tintasepintura/app/ui/login/login_repository.dart';
import 'package:tintasepintura/core/utils/app_utils.dart';
import 'package:tintasepintura/core/values/app_enum.dart';
import 'package:tintasepintura/core/values/constants.dart';
import 'package:tintasepintura/routes/app_routes.dart';

class LoginViewModel extends GetxController with AppUtils {
  final repository = Get.find<LoginRepository>();

  String email = "";
  String password = "";
  RxBool isValidEmail = false.obs;
  RxBool isPassword = false.obs;
  RxBool userFirebaseLoginNull = false.obs;
  RxBool isLogin = true.obs;
  RxBool isLoading = false.obs;
  RxString messageError = "".obs;

  toggleAuthMode() {
    isLogin.value = !isLogin.value;
  }

  changeEmail(String value) {
    email = value;
    messageError.value = "";
    isValidEmail.value = GetUtils.isEmail(value);
  }

  changePassWord(String value) {
    password = value;
    messageError.value = "";
    isPassword.value = value.length >= 6;
  }

  submit() async {
    messageError.value = "";
    isLoading.value = true;
    if (isLogin.value) {
      final response = await repository.sign(email: email, password: password);
      if (response == AuthStatusMessage.loggedInSuccessfully) {
        Get.offNamed(AppRoutes.navigationControl);
      } else if (response == AuthStatusMessage.userNotFound) {
        messageError.value = AppMessageError.userNotFound;
      } else {
        messageError.value = AppMessageError.genericError;
      }
    } else {
      final response =
          await repository.signup(email: email, password: password);
      if (response == AuthStatusMessage.successfullyCreated) {
        isLogin.value = true;
      } else if (response == AuthStatusMessage.existingAccount) {
        messageError.value = AppMessageError.existingAccount;
      } else {
        messageError.value = AppMessageError.genericError;
      }
    }
    email = "";
    password = "";
    isLoading.value = false;
  }

  RxBool firebaseInitialized = false.obs;
  Future initFirebase() async {
    try {
      await Firebase.initializeApp();
      firebaseInitialized.value = true;
      authStateChanges();
    } catch (e) {
      loggerError(message: e);
    }
  }


  authStateChanges() async {
    FirebaseAuth.instance.authStateChanges().listen(
      (User? user) {
        if (user != null) {
          loggerInfo(message: "USUÁRIO: $user");
          Get.offNamed(AppRoutes.navigationControl);
        }else{
          userFirebaseLoginNull.value = true;
        }
      },
    );
  }


  @override
  void onInit() {
    super.onInit();
    initFirebase();
  }

  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }
}
