class User {
  String id;
  String userName;
  String email;

  User({
    required this.id,
    required this.email,
    required this.userName});
}