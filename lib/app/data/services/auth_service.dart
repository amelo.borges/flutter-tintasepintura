import 'package:firebase_auth/firebase_auth.dart';
import 'package:tintasepintura/core/utils/app_utils.dart';
import 'package:tintasepintura/core/values/app_enum.dart';

class AuthService with AppUtils{
  AuthService._internal();
  static final AuthService _instance = AuthService._internal();
  static AuthService get instance => _instance;



  Future<AuthStatusMessage> sign({required String email, required String password}) async{
    try {
      await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: email,
          password: password
      );
      return AuthStatusMessage.loggedInSuccessfully;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        loggerError(message: 'Nenhum usuário encontrado para esse e-mail.');
        return AuthStatusMessage.userNotFound;
      } else if (e.code == 'wrong-password') {
        loggerError(message: 'Senha incorreta fornecida para esse usuário.');
        return AuthStatusMessage.incorrectPassword;
      }
    }
     return  AuthStatusMessage.genericError;
  }




  Future<AuthStatusMessage> signup({required String email, required String password}) async{
    try {
      await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
      return AuthStatusMessage.successfullyCreated;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        loggerError(message: 'A senha fornecida é muito fraca.');
        return AuthStatusMessage.weakPassword;
      } else if (e.code == 'email-already-in-use') {
        loggerError(message: 'A conta já existe para esse e-mail.');
        return AuthStatusMessage.existingAccount;
      }
    }
      return AuthStatusMessage.genericError;
  }
}
