import 'dart:ui';

class AppImages {
  static const String wallLeft = "assets/images/wall_left.png";
  static const String wallRight = "assets/images/wall_right.png";
  static const String wallTop = "assets/images/wall_top.png";
  static const String wallBottom = "assets/images/wall_bottom.png";
  static const String logoApp = "assets/images/logo_app.png";
  static const String backbround = "assets/images/background.png";
  static const String logoAppbar = "assets/images/logo_appbar.png";
  static const String iconCancel = "assets/images/icon_cancel.png";
}

class AppMessageError{
  static const String wallSize = "A área da parede não pode ser menor ou igual a 1m² e maior que 50m².";
  static const String wallSmallerThanTheDoor = "A parede tem que ter no mínimo 30cm a mais que a porta.";
  static const String portalAndWindowsSizeLimit = "As áreas das portas e janelas não podem ultrapassar 50% da área da parede.";
  static const String userNotFound = "Usuário não encontrado!";
  static const String genericError = "Algo deu errado!";
  static const String existingAccount = "Contá já existente.";
}

class AppColors {
  static final List<Color> setColors = [
    const Color.fromRGBO(5,154,222,0.6),
    const Color.fromRGBO(8,232,234,0.6),
    const Color.fromRGBO(43,224,129,0.6),
    const Color.fromRGBO(150,233,83,0.6),
    const Color.fromRGBO(232,219,0,0.6),
    const Color.fromRGBO(242,173,69,0.6),
    const Color.fromRGBO(233,82,0,0.6),
    const Color.fromRGBO(231,56,85,0.6),
    const Color.fromRGBO(225,83,155,0.6),
    const Color.fromRGBO(227,82,211,0.6),
    const Color.fromRGBO(127,109,231,0.6),
    const Color.fromRGBO(91,158,245,0.6),
  ];
}