enum AuthStatusMessage {
  userNotFound,
  incorrectPassword,
  weakPassword,
  existingAccount,
  successfullyCreated,
  loggedInSuccessfully,
  genericError,
}