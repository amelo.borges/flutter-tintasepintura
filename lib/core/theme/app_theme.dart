import 'package:flutter/material.dart';

class AppTheme {
  static List<BoxShadow> shadow(Color color) {
    return [
      BoxShadow(
        color: color,  //Color.fromRGBO(50, 132, 229, 0.10),
        spreadRadius: 4,
        blurRadius: 7,
        offset: const Offset(0,5),
      ),
    ];
  }
}




